#include "spjoy.h"
#include <iostream>

spJoy::spJoy(int buttons)
{
    dev = libevdev_new();
    libevdev_set_name(dev,"MyGameInput");
    libevdev_set_id_bustype(dev, BUS_USB);
    libevdev_set_id_vendor(dev, 0x3);
    libevdev_set_id_product(dev, 0x3);
    libevdev_set_id_version(dev, 2);

    libevdev_enable_event_type(dev, EV_ABS);
    absinfo.value=0;
    absinfo.maximum=460;
    absinfo.minimum=-460;
    absinfo.fuzz=0;
    absinfo.flat=0;
    for (int i=0; i < 6; i++){
        libevdev_enable_event_code(dev, EV_ABS, i, &absinfo);
    }

    if (buttons > 0)
    {
        libevdev_enable_event_type(dev,EV_KEY);
        for (int i=0; i < buttons; i++)
            if (bnum2ev_code(i))
                libevdev_enable_event_code(dev, EV_KEY, bnum2ev_code(i), NULL);
    }

    err = libevdev_uinput_create_from_device(dev, LIBEVDEV_UINPUT_OPEN_MANAGED, &uidev);
    if (err != 0){
        std::cout<<"error"<<std::endl;
        exit(err);
    }
}

spJoy::~spJoy(){
    libevdev_uinput_destroy(uidev);
}

void spJoy::send_motion(int *motion){
    for (int i=0; i<6 ; i++){
        libevdev_uinput_write_event(uidev,EV_ABS,i,motion[i]);
    }
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    std::cout<<"sending motion event"<<std::endl;
}

void spJoy::send_buttons(int bnum, bool pressed){
    if (bnum2ev_code(bnum)){
        libevdev_uinput_write_event(uidev, EV_KEY, bnum2ev_code(bnum), pressed);
        libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
        std::cout<<"sending button event"<<std::endl;
    }
}

int spJoy::bnum2ev_code(int bnum){
    if (bnum < 12)
        return bnum + 0x120; // BTN_JOYSTICK - BTN_BASE6
    else return -1;
}



