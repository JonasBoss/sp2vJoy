#include <iostream>
#include <spnav.h>
#include <signal.h>
#include <chrono>

#include <thread>
#include <mutex>
#include <condition_variable>
#include "spjoy.h"

using namespace std;

enum AXIS{
    X =0, Y =1, Z =2,
    RX=3, RY=4, RZ=5,
};

// threading stuff
mutex m, event_lck; // mutex for thread syncronisation and exclusive acsess to motion[]
condition_variable cv;

int motion[6] = {0,0,0, 0,0,0};
int last_motion[6] = {0,0,0, 0,0,0};

struct spnav_event_button seb;
bool is_button_event = false;

int fd = spnav_fd();

void sig(int s)
{
    cout << "closing connection to spacenav deamon"<<endl;
    spnav_close();
    terminate();
}

void motion_to_array(spnav_event_motion ev, int motion_array[6] )
{
    motion_array[X] = ev.x;
    motion_array[Y] = ev.y;
    motion_array[Z] = ev.z;
    motion_array[RX] = ev.rx;
    motion_array[RY] = ev.ry;
    motion_array[RZ] = ev.rz;
}

void read_spnav()
{
    spnav_event sev;
    if (spnav_open() == -1)
    {
        cout << "failed to connect to the space navigator daemon" << endl;
    }
    spnav_sensitivity(1);

    while (spnav_wait_event(&sev)) {
        event_lck.lock();
        if (sev.type == SPNAV_EVENT_MOTION)
            motion_to_array(sev.motion, motion);
        else
        {
            seb = sev.button;
            is_button_event = true;
        }
        event_lck.unlock();
        cv.notify_one();
    }
}

bool is_motion_event()
{
    for (int i=0; i < 6; i++)
    {
        if (last_motion[i] != motion[i])
            return true;
    }
    return false;
}


int main()
{
    signal(SIGINT, sig);
    spJoy joy(21);
    cv_status status;
    thread th1(read_spnav);

    while (1)
    {
        //wait for thread 1 until time elapsed
        unique_lock<mutex> lk(m);
        status = cv.wait_for(lk,chrono::microseconds(17000));
        event_lck.lock();
        if ( status == cv_status::timeout) // create zero event if timeout
            for (int i=0; i<6; i++)
                motion[i] = 0;

        if (is_motion_event())
        {
            joy.send_motion(motion);
            for (int i=0; i < 6; i++)
                last_motion[i]=motion[i];
        }
        else if (is_button_event)
        {
            joy.send_buttons(seb.bnum,seb.press);
            is_button_event = false;
        }
        event_lck.unlock();
        lk.unlock();
    }
    return 0;
}
