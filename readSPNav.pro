TEMPLATE = app
CONFIG += console c++11
CONFIG += thread
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -L/usr/include/libevdev-1.0/ -levdev
LIBS += -L/usr/include/spnav -lspnav
SOURCES += \
        main.cpp \
        spjoy.cpp

HEADERS += \
    spjoy.h
