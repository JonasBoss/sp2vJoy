#ifndef SPJOY_H
#define SPJOY_H
#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>
#include <libinput.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <linux/uinput.h>

class spJoy
{
private:
    int err;
    struct libevdev *dev;
    struct libevdev_uinput *uidev;
    struct input_absinfo absinfo;

    static int bnum2ev_code(int bnum);


public:
    spJoy(int buttons);
    ~spJoy();
    void send_motion(int motion[6]);
    void send_buttons(int bnum, bool pressed);
};

#endif // SPJOY_H
